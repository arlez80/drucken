# drucken

HSPでBDF形式のフォントを表示するモジュールです。

HSP3Dish向けです。AndroidやiOS出力で日本語出したいがために作りました。

# usage

```
#!hsp

drk_load "font.drk", bdf
drk_mes bdf, "こんにちは。"

```

位置変更、色の変更はmesと同じでposやcolorが使用できます。

# fontについて

shnm6x12a.bdfとshnmk12maru.bdfは、東雲フォントです。以下のURLから入手可能です。

http://openlab.ring.gr.jp/efont/shinonome/

font.drkは上記フォントから生成しました。

# license

MIT License